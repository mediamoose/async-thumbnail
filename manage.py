#!/usr/bin/env python
import sys


if __name__ == "__main__":

    from django.conf import settings
    from django.core.management import execute_from_command_line

    settings.configure(
        ASYNC_THUMBNAIL_PATTERN_NAME="render",
        DATABASES={"default": {"ENGINE": "django.db.backends.sqlite3"}},
        CACHES={"default": {"BACKEND": "django.core.cache.backends.dummy.DummyCache"}},
        INSTALLED_APPS=[
            "async_thumbnail",
            "django.contrib.auth",
            "django.contrib.contenttypes",
            "sorl.thumbnail",
        ],
        MIDDLEWARE_CLASSES=(),
        ROOT_URLCONF="async_thumbnail.urls",
    )

    execute_from_command_line(sys.argv)
