ARG PYTHON_VERSION="3.7"

FROM python:$PYTHON_VERSION

WORKDIR /package/

COPY requirements.txt /package/requirements.txt
RUN pip install -r requirements.txt

ARG PIP_DJANGO="django"
RUN pip install $PIP_DJANGO

COPY . /package

RUN pip install /package

RUN mkdir -p coverage
