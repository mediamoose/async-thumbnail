#!/usr/bin/env python

import sys

from django.conf import settings
from django.test.utils import get_runner


settings.configure(
    ASYNC_THUMBNAIL_PATTERN_NAME="render",
    DATABASES={"default": {"ENGINE": "django.db.backends.sqlite3"}},
    CACHES={"default": {"BACKEND": "django.core.cache.backends.dummy.DummyCache"}},
    INSTALLED_APPS=[
        "async_thumbnail",
        "django.contrib.auth",
        "django.contrib.contenttypes",
        "sorl.thumbnail",
    ],
    MIDDLEWARE_CLASSES=(),
    ROOT_URLCONF="async_thumbnail.urls",
)

try:
    import django

    setup = django.setup
except AttributeError:
    pass
else:
    setup()


def run_tests(*test_args):
    if not test_args:
        test_args = ["tests"]

    # Run tests
    TestRunner = get_runner(settings)
    test_runner = TestRunner()

    failures = test_runner.run_tests(test_args)

    if failures:
        sys.exit(bool(failures))


if __name__ == "__main__":
    run_tests(*sys.argv[1:])
